# Go-CD Spike

This is a test project to set up a go-cd server and a number of agents. Both the server and the agents are dockerized. The server is configured to load an example pipeline from https://gitlab.com/leopard2a5-knorke/example-pipelines . The agents are installed with an oracle java 8 JDK and maven 3.

You can create the setup with

```
ansible-playbook -i inventory provision.yml
```

To tear everything down just do
```
docker rm -vf go-server go-agent-01 go-agent-02 go-agent-03
```

Access the server at http://localhost:8153/go .